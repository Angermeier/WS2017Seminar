\documentclass{IEEEtran}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{natbib}
\usepackage{url}
\usepackage{pgfplots}
\pgfplotsset{compat=1.13}
\usepackage{tikz}
\usepackage{lmodern}
\usepackage{hyperref}

\pgfplotscreateplotcyclelist{mycolorlist}{%
red,every mark/.append style={fill=red!80!black},mark=none\\%
blue,every mark/.append style={fill=blue!80!black},mark=none\\%
black,every mark/.append style={fill=black},mark=none\\%
green!60!black,every mark/.append style={fill=green!80!black},mark=none\\%
}

\def\UrlBreaks{\do\/\do-\do.\do\#}

%allow more space between the words to prevent overfull boxes
\setlength{\emergencystretch}{1em}

\bibliographystyle{abbrvnat}
\setcitestyle{authoryear,round}

\title{Image Super Resolution \\ \Large eine Implementierung in Tensorflow}
\author{Thomas Angermeier\\
Hochschule für angewandte Wissenschaften\\
München, Deutschland\\
Email: \href{mailto:thomas.angermeier@hm.edu}{thomas.angermeier@hm.edu}}
\date{\today}

\begin{document}
\maketitle
\section{Abstrakt}
Image Super Resolution bezeichnet Verfahren, um die Auflösung von Bildern zu erhöhen.
Die Verfahren können z.B. angewendet werden, um Bilddateien die für eine Übertragung komprimiert wurden am Ziel wieder hochzurechnen.
Dabei kann bei einer 2 fachen Vergrößerung bis zu 75\% der Bandbreite eingespart werden.
In dieser Arbeit werden Verfahren untersucht, die für eine Steigerung der Auflösung verwendet werden können.
Insbesondere Verfahren, die auf Maschinellem Lernen aufbauen, werden untersucht.

Ein solches Verfahren wurde mit Hilfe von Tensorflow implementiert.
Tensorflow ist eine Open Source Softwarebibliothek für Maschinelles Lernen.
Die Bibliothek wurde 2015 vom Google Brain Team veröffentlicht \citep{tensorflow2015-whitepaper}.

\section{Einführung}
Je höher die Auflösung eines Bildes, desto mehr Information stehen darauf zur Verfügung.
Wenn die Auflösung eines Bildes erhöht werden soll, muss die zusätzliche Information geschätzt werden.
Das kann durch lokale Interpolation aus benachbarten Pixeln geschehen (z.B. bilineare Interpolation und bikubische Interpolation).
Ein anderer Ansatz sind Verfahren, welche die Zusatzinformationen z.B. aus ähnlichen Bildern nehmen.
Die Zusatzinformation soll anhand von anderen Bildern trainiert werden. 
Anwendungsbereiche für Image Super Resolution sind unter anderem die Medizin, Astrofotografie, forensische Analyse von Aufnahmen aus Überwachungskameras, sowie die Hochskalierung nach einer Komprimierung zur schnelleren Datenübertragung.
Für Videoskalierung können in Echtzeitanwendungen einfache Algorithmen zur Erhöhung der Auflösung verwendet werden.

Ziel dieser Arbeit ist es zu untersuchen, ob man mit einfachen Machine Learning Mitteln ein Verfahren zur Hochskalierung von Bildern entwickeln kann, welches besser funktioniert als die klassische lokale Interpolation.

\section{Verwandte Arbeiten}
Ein Verfahren Image Super Resolution mittels Machine Learning ist SRCNN \citep{DBLP:journals/corr/DongLHT15}.
Hier wurde ein Neuronales Netz angelernt, um die Auflösung zu verbessern.
Dazu wird eine einfache Formel zur Super Resolution aufgestellt und gezeigt dass diese durch ein Convolutional Netz abbildbar ist.
Das Projekt wurde zuerst in MatLab und in Version 2 dem Deep Learning Framework Caffe implementiert und ist zum Download verfügbar.

Ein verwandtes Thema wurde mit Pixel Recursive Superresolution \citep{DBLP:journals/corr/DahlNS17} umgesetzt.
Hier werden aus 8x8 großen Bildern 64x64 große Bilder generiert.
Das funktioniert, da ein Neuronales Netz angelernt wurde, spezifische Objekte auf den Bildern zu erkennen (z.B. Gesichter von Menschen).
Da bei 8x8 Pixeln nicht viel Information über das Objekt auf dem Bild enthalten ist, ist es essentiell zu wissen, was auf dem Bild zu sehen ist.

Einen anderen Ansatz hat das Google Brain Team um Yaniv Romano entwickelt.
Rapid and Accurate Image Super Resolution (RAISR) \citep{DBLP:journals/corr/RomanoIM16} setzt bei der Hochrechnung der Auflösung auf Kantenerkennung.

Eine weitere Möglichkeit Super Resolution zu lösen zeigen \cite{Yang:2010:ISV:1892456.1892463}.
Hier wird in einer Lernphase ein Verzeichnis aus gering- und hochaufgelösten Bildteilen generiert.
Aus diesen Bildteilen wird dann aus gering aufgelösten Bildern hoch aufgelöste generiert.
Dabei werden auch die Unterschiede zwischen verschieden großen Verzeichnissen untersucht.

Auf dieser Methodik aufbauend werden weiterhin Verfahren entwickelt z.B. \cite{NingSR}.

Mit \cite{DBLP:journals/corr/LedigTHCATTWS16}, \cite{DBLP:journals/corr/KimLL15b} und \cite{DBLP:journals/corr/HeZR014} existieren weitere auf Neuronalen Netzen basierende Image Super Resolution Verfahren.

Bei diesen Verfahren wird oft mit dem YCbCr Farbraum gearbeitet.
Dabei wird die Y Komponente (Beleuchtung) aufwändig bearbeitet und die Cb Cr Komponenten, z.B. bikubisch, interpoliert.
Das menschliche Auge nimmt die Beleuchtungskomponente (Y) als Schärfe wahr.
Die Farbkomponenten (Cb und Cr) können deshalb ohne menschlich sehbare einbußen interpoliert werden.

\section{Implementierung}
Das Neuronale Netz in Tensorflow (vgl. \autoref{img:tensorflow_complete}) besteht aus einem Eingangslayer, zwei hidden Layer und einem Ausgangslayer.
Die drei Hauptbereiche des Programms in \autoref{img:tensorflow_complete} sind:
\begin{enumerate}
\item dnn
\item train
\item eval
\end{enumerate}
Solche Bereiche können in Tensorflow angelegt werden und dienen der übersichtlicheren Darstellung des Programms in Tensorboard.

\begin{figure*}[t]
  \centering
  \includegraphics[width=\textwidth]{../images/complete.png}
  \caption{Tensorboard Visualisierung des Tensorflow Programms}
  \label{img:tensorflow_complete}
\end{figure*}
\begin{figure}[H]
  \centering
  \includegraphics[width=\columnwidth]{../images/dnn.png}
  \caption{Tensorflow Neuronales Netz mit Hidden- und Ouputlayer, Mittlerer Quadratischer Abweichung}
  \label{img:tensorflow_dnn}
\end{figure}

Das eigentliche Neuronale Netz wurde im Bereich ``dnn'' angelegt und ist in \autoref{img:tensorflow_dnn} vergrößert dargestellt.
Hier sieht man die Layers ``hidden1'', ``hidden2'' und ``outputs''.
In ``hidden1'' werden aus $10 * 10 = 100$ Eingangswerten 200 Ausgangswerte generiert.
In ``hidden2'' wird der Output von ``hidden1'' verarbeitet und auf 300 Werte gemappt.
Im letzten Layer ``outputs'' werden aus dem Output von ``hidden2'' die $20 * 20 = 400$ Ausgangswerte berechnet.
Dabei kommt man für das gelernte Netz auf eine zweifache Vergrößerung.

Als Schnittstelle zwischen dem Tensorflowprogramm und dem Rahmenprogramm bietet Tensorflow sog. Variablen.
In die Variable ``x'' (zu sehen in \autoref{img:tensorflow_complete}, zwischen ``hidden2'' und ``outputs'') werden zur Laufzeit die zu vergrößernden Pixelpatches geladen.
Die Variable ``y'' (zu sehen zwischen ``hidden1'' und ``hidden2'') wird zur Laufzeit das Ergebnis, welches das Netz liefern soll geladen.

\textbf{Training}

Die Trainingsbilder werden eingelesen, in 10x10 Pixel große Teile zerlegt (Überschneidung jeweils 5 Pixel in beiden Dimensionen).
$ patch\_size\_x = 10, patch\_size\_y = 10, stride\_x = 5, stride\_y = 5  $
In \autoref{img:explain_stride} ist beispielhaft eine 15x15 große Matrix zu sehen.
Die Quadrate in Rot, Grün, Blau und Orange beschreiben die vier 10x10 Patches, welche aus der Matrix generiert worden wären.
Die Überschneidung ist in dieser Abbildung ebenfalls gut zu sehen (Rechtecke mit verschieden farbigen Kanten).

\begin{figure}[H]
  \centering
  \resizebox {0.5\columnwidth} {!} {
    \begin{tikzpicture}
      \draw [step=1.0,gray, very thin] (0,0) grid (15,15);
      \draw [step=1.0,blue, very thick] (0,0) rectangle (10,10);
      \draw [step=1.0,orange, very thick] (5,0) rectangle (15,10);
      \draw [step=1.0,red, very thick] (0,5) rectangle (10,15);
      \draw [step=1.0,green, very thick] (5,5) rectangle (15,15);
    \end{tikzpicture}
  }
  \caption{Generierung der Patches mit Überschneidung}
  \label{img:explain_stride}
\end{figure}

D.h. aus einem 160x240 oder 240x160 Pixel großen Bild entstehen somit $(160 / patch\_size\_x * patch\_size\_x / stride\_x - 1)*(240 / patch\_size\_y * patch\_size\_y / stride\_y - 1) = 1457$ Trainingpatches.
Bei 400 Trainingsbilder kommt man insgesamt auf 582800 Trainingspatches, die dem Neuronalen Netz pro Epoche zum Lernen zur Verfügung stehen.
Analog dazu wird die ``Wahrheit'' aus den 320x480 und respektiv 480x320 großen Bildern generiert.
Diese überschneiden sich um jeweils 10px in beide Dimensionen.

Beim Training des Netzes wird der Mean Square Error aus dem Ergebnis und der Wahrheit minimiert (zu sehen in \autoref{img:tensorflow_dnn}, rechts oben im Bild).
Der Mean Square Error wird wie folgt berechnet $ MSE = (out-truth)^2 $.
Die mittlere quadratische Abweichung ist eine sinnvolle zu minimierende Messgröße, da sie immer positiv ist.
Bei einem wahren Wert von 5 ist der quadratische Abstand für die ``Schätzungen'' 3 und 7 gleich 4.
Der Abstand selbst wäre -2 bzw. 2, was sich schlecht minimieren lässt.
Zum minimieren des Mean Square Errors kommt ein Adam Optimizer zum Einsatz.
Zu sehen in \autoref{img:tensorflow_train}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\columnwidth]{../images/train.png}
  \caption{Tensorflow Trainingsgoals}
  \label{img:tensorflow_train}
\end{figure}

Während dem Training werden mittels des Tensorflow Loggingmechanismus mehrere Werte zur späteren Visualisierung mit Tensorboard abgespeichert.
In diesem Fall werden pro Epoche der mittlere absolute Fehler, der maximale Fehler und der minimale Fehler als ``Skalare'' abgespeichert (die Berechnung ist zu sehen in \autoref{img:tensorflow_eval}).

\begin{figure}[H]
  \centering
  \includegraphics[width=\columnwidth]{../images/eval.png}
  \caption{Tensorflow Evaluation Goals}
  \label{img:tensorflow_eval}
\end{figure}

Der pixelweise berechnete Fehler wird für jede Epoche als Histogramm abgespeichert.

Nach jeder Epoche wird der gelernte Zwischenstand über einen Saver gespeichert.
Standardmäßig erlaubt Tensorflow hier das Abspeichern der letzten 5 Stände, dieser Wert kann allerdings bei der Initialisierung des Savers übergeben werden.

Der Sourcecode ist in dem Repository \cite{tangerme:repo} verfügbar.

\textbf{Evaluation}

Analog zum einlesen der Trainingsdaten werden die Testdaten eingelesen und im Arbeitsspeicher zwischengespeichert.
Jedes Bild wird einzeln in das Neuronale Netz gespeist und das Ergebnis wieder zu einem großen Bild gemerged.
Bei der Evaluation werden mit den Testdaten die selben Skalare abgespeichert, wie während dem Training aufgrund der Trainingsdaten.
Aus dem Neuronalen Netz werden für die im Wertebereich 0 bis 255 befindlichen Pixelwerte teilweise Werte kleiner 0 und größer 255 für ``vorgeschlagen''.
Da dies nicht korrekt sein kann werden verschiedene Korrekturen, jeweils einzeln, vorgenommen.
Zu jeder Korrektur wird ebenfalls mittlere, maximale und minimale absolute Abweichung über den Tensorflow Logging Mechanismus abgespeichert.
Die Vorgenommenen Korrekturen werden im Folgenden kurz beschrieben.
\begin{enumerate}
\item Die Werte kleiner Null werden auf Null gesetzt, die Werte größer 255 respektiv auf 255.
\item Alle Werte werden um den größten negativen Wert ins positive verschoben und anschließend auf den Bereich Null bis 255 normiert.
\end{enumerate}

\textbf{Benutzung}

In dem Netz wird der Beleuchtungsanteil (Y im YCbCr Farbraum) hochskaliert.
Die beiden weiteren (weniger Information liefernden) Komponenten werden bikubisch interpoliert.
Die mit dem Neuronales Netz hochskalierten Patches werden wieder miteinander gemerged.
Dabei werden die Werte der mehrfach berechneten Pixel gemittelt.
Die interpolierten Komponenten werden wiederum mit der gemergeten Komponente zu einem kompletten Bild zusammengesetzt und abgespeichert.

\section{Evaluation}
\iffalse
\begin{itemize}
\item Verwendung BSDS500
\item Vergleich Implementierung mit min. RAISR \url{https://github.com/movehand/raisr} \url{https://github.com/HerrHao/RAISR} oder \url{https://github.com/volvet/RAISR-1}, Original Implementierung SRCNN in Matlab + Caffe \url{http://mmlab.ie.cuhk.edu.hk/projects/SRCNN.html}
\item Laufzeitvergleich aufgeteilt in Lernphase und Anwendung des gelernten
\item Vergleich Exaktheit (Verfahren noch suchen)
\end{itemize}
\fi

Als Referenz der Laufzeit:
Das System auf dem das Training stattfand war wie folgt zusammengestellt:
\begin{itemize}
\item Software: Python 3.5, Tensorflow v1.4 auf cuDNN v6.0, CUDA v8.0 und Linux Mint
\item Prozessor: Intel i5 4670 4x3.4 - 3.8Ghz
\item Grafikkarte: Zotac GTX1060 3GB GDDR5 (8008Mhz)
\item Arbeitsspeicher: 16GB 1600Mhz DDR3
\end{itemize}

Als Datensatz zum Trainieren und Testen wurde der aus 500 Bildern bestehende BSDS500 \citep{amfm_pami2011} verwendet.
Die Bilder wurden in Trainings- und Testdatensatz aufgeteilt.
400 der Bilder wurden zum trainieren des Netzes verwendet.
Die restlichen 100 dienen als Testdaten.

Beim Trainieren fällt auf, dass nach ca 500 Epochen kein weiterer Fortschritt mehr stattfindet.
Das Trainieren von 1.000 Epochen dauert auf dem beschriebenen System ca. 1600 Sekunden.

Bei dem Training liegt (laut NVidia Control Center) die Auslastung der Grafikkarte bei rund 45\%.
Das Verändern der Anzahl der Patches, welche gleichzeitig auf der Grafikkarte bearbeitet werden führt zu keiner Veränderung der Auslastung der Grafikkarte
Auch die Zeit in der das Training beendet wird verändert sich nicht.

Die hier zu sehenden Grafiken sind generiert aus mit Tensorflow geloggten Daten.
In Tensorboard können diese Daten als z.B. CSV exportiert werden.
In den folgenden Grafiken ist jeweils der Fehler im Verlauf der Epochen dargestellt.
Die rote Linie ist zum Vergleich der selbe Fehler der Bikubischen Interpolation.
Der minimale Fehler lag bei jeder Variante bei 0, was weniger überraschend ist, da dazu nur ein einziger Pixelwert exakt getroffen werden musste.

Im folgenden werden Ausschnitte eines Bildes gezeigt.
Das Originalbild ist in \autoref{img:butterfly:marked} zu sehen.
Der Bereich welcher weiterhin zu sehen ist, ist rot markiert.

\begin{figure}[H]
  \centering
  \minipage{0.49\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/butterfly_marked.jpg}
    \caption{Originalbild des Schmetterlings}
    \label{img:butterfly:marked}
  \endminipage
\end{figure}

\textbf{Ergebnis der Neuronalen Netzes:}

Hier wird der Output des Neuronalen Netzes direkt mit der Wahrheit verglichen.
\autoref{img:butterfly:orig} bis \autoref{img:butterfly:super:diff} zeigen das Bild eines Schmetterlings.
\autoref{img:butterfly:orig} zeigt das Originalbild. \autoref{img:butterfly:super} zeigt das Ergebnis (welches etwas zu dunkel erscheint) des Neuronalen Netzes.
\autoref{img:butterfly:super:diff} zeigt die Differenz der beiden Bilder.
Das dunkle Grau bedeutet die meisten Pixel werden relativ gut getroffen.
Die weißen Linien an den Kanten zeigen an diesen einen großen Fehler.

\begin{figure}[H]
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterfly.jpg}
    \caption{Original- bild}
    \label{img:butterfly:orig}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper.jpg}
    \caption{Output des NN}
    \label{img:butterfly:super}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper_diff.jpg}
    \caption{Differenz der beiden}
    \label{img:butterfly:super:diff}
  \endminipage
\end{figure}

In \autoref{img:bare_max_error} sieht man den maximalen Fehler.
Die rote Linie, wie bereits erwähnt, der maximale Fehler der Bikubischen Interpolation.
Dieser liegt, da die bikubische Interpolation nicht lernend ist, stehts bei 15,78.
Blau zu sehen ist der maximale Fehler der pro Epoche auf den Trainingsdaten.
Dieser startet bei ca. 533 pendelt sich aber relativ schnell bei 166 ein.
Interessant zu sehen ist, in der Evaluation starten die maximale Abweichung bei 440 (also signifikant geringer als auf den Trainingsdaten), pendelt sich dann bei 170 ein (also geringfügig höher als auf den Trainingsdaten).

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,15.78) (999,15.78) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/train-max.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-max.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{maximaler Fehler}
  \label{img:bare_max_error}
\end{figure}

\autoref{img:bare_mean_error} zeigt den mittleren Fehler.
Auch hier ist die rote Linie der mittlere Fehler der bikubischen Interpolation.
Dieser liegt auf den Trainingsdaten bei 4,45.
Auf den Trainingsdaten (blauer Graph) liegt nach ca. 400 Epochen bei 10,8.
Die gemittelten Testdaten verbessern den mittleren Fehler auf 6.

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,4.449) (999,4.449) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/train-mean.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-mean.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{durchschnittlicher Fehler}
  \label{img:bare_mean_error}
\end{figure}

\textbf{Korrektur 1:}

Hier wird die erste Korrektur (negative Werte auf 0 und Werte $>$ 255 auf 255 setzen) genauer betrachtet.


Der maximale Fehler (grüner Graph in \autoref{img:corr_max_error}) sinkt durch diese Korrektur von 170 auf 167.
Der durchschnittliche Fehler (grüner Graph in \autoref{img:corr_mean_error}) bleibt unverändert auf 6.
Das liegt daran, dass ca. genauso viele Werte über 255 auf 255 und unter Null auf Null gesetzt wurden.
Daraus folgt der durchschnitt bleibt gleich.

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,15.78) (999,15.78) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/train-max.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-max.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-corr-max.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{maximaler Fehler - Korrektur 1}
  \label{img:corr_max_error}
\end{figure}

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,4.449) (999,4.449) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/train-mean.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-mean.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-corr-mean.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{durchschnittlicher Fehler - Korrektur 1}
  \label{img:corr_mean_error}
\end{figure}

\begin{figure}[H]
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterfly.jpg}
    \caption{Origi- nalbild}
    \label{img:butterfly:orig2}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper2.jpg}
    \caption{Ergeb- nis Korrektur 1}
    \label{img:butterfly:super2}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper2_diff.jpg}
    \caption{Differenz der beiden}
    \label{img:butterfly:super2:diff}
  \endminipage
\end{figure}

\autoref{img:butterfly:orig2} bis \autoref{img:butterfly:super2:diff} zeigen das selbe Bild des Schmetterlings, diesmal wie erwähnt korrigiert.
Dabei fällt auf:
Das korrigierte Bild sieht genauso aus, wie das Originalbild.
Die Differenz (\autoref{img:butterfly:super2:diff}) sieht der Differenz in \autoref{img:butterfly:super:diff} sehr ähnlich.

\textbf{Korrektur 2:}
Die Ergebnisse der zweiten Korrektur (alle Werte um negativsten Wert ins Positive verschieben, danach auf Bereich 0-255 beschränken) werden in diesem Abschnitt beschrieben.

Das korrigierte Bild des Schmetterlings (\autoref{img:butterfly:hist}) sieht dem Originalbild (\autoref{img:butterfly:orig3}) sehr ähnlich.
Lediglich in der Differenz fällt auf, dass durch das Verschieben der Werte viele Bereiche einen höheren Fehler aufweisen.

\begin{figure}[H]
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterfly.jpg}
    \caption{Origi- nalbild}
    \label{img:butterfly:orig3}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflyhist.jpg}
    \caption{Ergeb- nis Korrektur 2}
    \label{img:butterfly:hist}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflyhist_diff.jpg}
    \caption{Diffe- renz Korrektur 2}
    \label{img:butterfly:hist:diff}
  \endminipage
\end{figure}

Diese Beobachtung spiegelt sich auch im durchschnittlichen Fehler (grüner Graph in \autoref{img:corr2_mean_error}) wieder.
Dieser steigt von ursprünglich 6 auf zwischenzeitlich 10, dann wieder 11 an.
Lediglich der maximale Fehler (grüner Graph in \autoref{img:corr2_max_error}) wird signifikant von 170 auf 144 verbessert.

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,15.78) (999,15.78) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/train-max.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-max.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-corr2-max.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{maximaler Fehler - Korrektur 2}
  \label{img:corr2_max_error}
\end{figure}

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,4.449) (999,4.449) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/train-mean.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-mean.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/eval-corr2-mean.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{durchschnittlicher Fehler - Korrektur 2}
  \label{img:corr2_mean_error}
\end{figure}

\begin{figure}[H]
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper_diff.jpg}
    \caption{Diffe- renz Originalbild}
    \label{img:butterfly:super:diff2}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper2_diff.jpg}
    \caption{Diffe- renz Korrektur 1}
    \label{img:butterfly:super2:diff2}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflyhist_diff.jpg}
    \caption{Diffe- renz Korrektur 2}
    \label{img:butterfly:hist:diff2}
  \endminipage
\end{figure}

Im direkten Vergleich der Differenzen sieht man, dass Korrektur 1 bei diesem Bild das beste Ergebnis liefert.
Die beste Korrektur liefert allerdings das mehrfach Berechnen der Pixelwerte und das anschließende Mitteln dieser.
Als Vergleich hier noch das Ergebnis und die Differenz der bikubischen Interpolation.
Dabei fällt eine leichte unschärfe des interpolierten Bildes auf.
Die Differenz zeigt neben den sehr gut getroffenen Pixeln auch sehr viele schlecht getroffene Pixel.

\begin{figure}[H]
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterfly.jpg}
    \caption{Diffe- renz Originalbild}
    \label{img:butterfly:orig4}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflycubic.jpg}
    \caption{Diffe- renz Korrektur 1}
    \label{img:butterfly:cubic}
  \endminipage\hfill
  \minipage{0.32\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflycubic_diff.jpg}
    \caption{Diffe- renz Korrektur 2}
    \label{img:butterfly:cubic:diff2}
  \endminipage
\end{figure}

Es existieren Bilder, bei denen keine der Korrekturen, noch das Neuronale Netz selbst ein gutes Ergebnis liefert.
Ein kleiner Überblick ist am Ende dieses Papers zu finden.

\textbf{Vergleich mit SRCNN:}

Hier folgt ein Vergleich mit einer Implementierung \citep{srcnn:repo} des SRCNN genannten verfahrens von \citep{DBLP:journals/corr/DongLHT15}.
Dort wird erwähnt man erreiche eine Genauigkeit dem Paper entsprechend nach 15.000 Epochen.
Das Trainieren der SRCNN Implementierung war mit dem mitgeliefertem Datensatz (bestehend aus 91 Bildern) nach 6 Stunden, 50 Minuten und 9 Sekunden erledigt.
Das Trainieren der oben genannten Implementierung dauerte für 15.000 Epochen 21 Stunden, 37 Minuten und 51 Sekunden.
Mit dem gleichen Datensatz (400 Bilder) hätte das Trainieren der SRCNN Implementierung über 75 Stunden benötigt.
Die Auslastung der Grafikkarte lag bei dem Trainieren der SRCNN Implementierung im Schnitt bei 85\%.

\begin{figure}[H]
  \minipage{0.49\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflyorig_bw.jpg}
    \caption{Originalbild}
    \label{img:butterfly:super:orig0}
  \endminipage\hfill
  \minipage{0.49\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper_bw.jpg}
    \caption{Output NN}
    \label{img:butterfly:super0}
  \endminipage
\end{figure}
\begin{figure}[H]
  \minipage{0.49\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflysuper2_bw.jpg}
    \caption{Korrektur 1}
    \label{img:butterfly:super20}
  \endminipage\hfill
  \minipage{0.49\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/butterflyhist_bw.jpg}
    \caption{Korrektur 2}
    \label{img:butterfly:hist0}
  \endminipage
\end{figure}
\begin{figure}[H]
  \centering
  \minipage{0.49\columnwidth}
    \includegraphics[width=\linewidth]{../images/data/crop/test_image.png}
    \caption{SRCNN}
    \label{img:butterfly:srcnn}
  \endminipage
\end{figure}

Im direkten Vergleich aller Bilder fällt auf, dass diese Implementierung ein sehr unscharfes Bild liefert.
Bei Betrachtung der Graphen zeigt während die in diesem Paper vrgestellte Implementierung sehr schnell auf einem Lernniveau stagniert, lernt die Implementierung von SRCNN weiterhin.
Der durchschnittliche Fehler sinkt, nachdem er bis ca. Epoche 2500 bei knapp über 40 stagnierte, weiter auf ca. 10 um dann weiter langsam auf schlussendlich 6,25 zu fallen.
Der bei 6,25 stagnierende Fehler liegt damit unmerklich über dem durchschnittlichen Fehler der hier vorgestellten Implementierung.
Im gleichen Schritt steigt der maximale Fehler von 136 wieder auf 200 und dann wieder auf 190 zu sinken.

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,15.78) (14999,15.78) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/long-max.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/srcnn-max.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{maximaler Fehler SRCNN}
  \label{img:compare_max_error}
\end{figure}

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,4.449) (14999,4.449) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/long-mean.csv};
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/srcnn-mean.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{durchschnittlicher Fehler SRCNN}
  \label{img:compare_mean_error}
\end{figure}

\iffalse
\begin{enumerate}
\item Histogram Matching (evtl. weglassen)
\end{enumerate}

\begin{figure}[H]
  \centering
  \resizebox{\columnwidth}{!}{
	\begin{tikzpicture}
 	  \begin{axis}[cycle list name=mycolorlist]
      \addplot +[mark=none] coordinates { (0,0) (999,0) };
	  \addplot table [x=Step, y=Value, col sep=comma, mark=none] {../data/train-min.csv};
	  \end{axis}
	\end{tikzpicture}  
  }
  \caption{minimaler Fehler}
  \label{img:min_error}
\end{figure}
\fi

\section{Erkenntnisse}
Es ist interessant zu sehen, mit welchen einfachen Mitteln man ein solch komplexes Problem lösen kann.
Tensorflow bietet einem dazu ein großes Spektrum an Machine Learning Werkzeugen.
Dazu gehören eine Großzahl an Optimierern und Werkzeuge zum Erstellen von Schichten in Neuronalen Netzen, sowie deren Benennung.
Auch der Logging Mechanismus und die Funktionen zum Speichern und Laden der erlernten Zwischenstände.

Dass man mit einem einfachen Neuronalen Netz schneller ähnliche Ergebnisse erzielen kann, wie mit einem komplexeren, länger zu trainierenden Netz ist ebenfalls interessant.

\begin{figure}[H]
\end{figure}

\bibliography{mybib}{}
%\clearpage
\begin{figure*}
  \minipage{0.19\textwidth}
    Originalbild
  \endminipage\hfill
  \minipage{0.19\textwidth}
    Ergebnis Bikubisch
  \endminipage\hfill
  \minipage{0.19\textwidth}
    Ergebnis Neuronales Netz
  \endminipage\hfill
  \minipage{0.19\textwidth}
    Ergebnis Korrektur 1
  \endminipage\hfill
  \minipage{0.19\textwidth}
    Ergebnis Korrektur 2
  \endminipage
\end{figure*}
\begin{figure*}
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/250087.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/250087cubic.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/250087super.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/250087super2.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/250087hist.jpg}
  \endminipage
\end{figure*}
\begin{figure*}
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/253055.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/253055cubic.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/253055super.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/253055super2.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/253055hist.jpg}
  \endminipage
\end{figure*}
\begin{figure*}
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/257098.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/257098cubic.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/257098super.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/257098super2.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/257098hist.jpg}
  \endminipage
\end{figure*}
\begin{figure*}
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/260058.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/260058cubic.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/260058super.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/260058super2.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/260058hist.jpg}
  \endminipage
\end{figure*}
\begin{figure*}
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/309040.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/309040cubic.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/309040super.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/309040super2.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/309040hist.jpg}
  \endminipage
\end{figure*}
\begin{figure*}
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/385039.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/385039cubic.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/385039super.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/385039super2.jpg}
  \endminipage\hfill
  \minipage{0.19\textwidth}
    \includegraphics[width=\linewidth]{../images/data/385039hist.jpg}
  \endminipage
\end{figure*}
\end{document}
