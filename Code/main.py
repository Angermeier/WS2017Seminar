import os
import time

import numpy as np
import scipy.misc as misc
import tensorflow as tf

import utils
from logger import Logger

n_inputs = 10 * 10  # 100
n_hidden1 = 200
n_hidden2 = 300
n_outputs = 20 * 20  # 400


def nn(mode='train', lowpath='images/test/low/', outpath='out/'):
    n_epochs = 1000
    x = tf.placeholder(tf.float32, shape=(None, n_inputs), name='x')
    y = tf.placeholder(tf.float32, shape=(None, n_outputs), name='y')

    with tf.name_scope('dnn'):
        hidden1 = tf.layers.dense(x, n_hidden1, name='hidden1', activation=tf.nn.relu)
        hidden2 = tf.layers.dense(hidden1, n_hidden2, name='hidden2', activation=tf.nn.relu)
        logits = tf.layers.dense(hidden2, n_outputs, name='outputs')
        loss = tf.reduce_mean(tf.square(y - logits))

    learning_rate = 1e-5
    with tf.name_scope('train'):
        optimizer = tf.train.AdamOptimizer(learning_rate)
        training_op = optimizer.minimize(loss)

    with tf.name_scope('eval'):
        square_diff = tf.abs(y - logits)
        square_maximum = tf.reduce_max(square_diff)
        square_minimum = tf.reduce_min(square_diff)
        square_mean = tf.reduce_mean(square_diff)
        # correct = tf.nn.in_top_k(logits, y, 1)
        # accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

    init = tf.global_variables_initializer()
    saver = tf.train.Saver(max_to_keep=0)

    if mode == 'train':
        low, high = utils.get_data('images/train/')

        low = list(map(lambda x: x.reshape(-1), low))
        high = list(map(lambda x: x.reshape(-1), high))

        # 1,2,4,5,8,10,16,20,25,31,40,47,50,62,80,94,100,124,155,188,200,235,248,310,376,400,470,496,620,752,775,940,1175,1240,1457,1550,1880,2350,2480,2914,3100,3760,4700,5828,6200,7285,9400,11656,12400,14570,18800,23312,29140,36425,58280,72850,116560,145700,291400,582800
        batch_size = 1880

        with tf.Session() as session:
            logger = Logger(outpath + '/train/', graph=session.graph)
            start_time = time.time()
            init.run()
            for epoch in range(n_epochs):
                epoch_maximum = 0.0
                epoch_minimum = 0.0
                epoch_mean = 0.0
                epoch_diff = []
                epoch_out = []
                for iteration in range(len(low) // batch_size):
                    x_batch = low[iteration * batch_size: (iteration + 1) * batch_size]
                    y_batch = high[iteration * batch_size: (iteration + 1) * batch_size]
                    e_mean, e_max, e_min, e_diff, e_out, _ = session.run(
                        [square_mean, square_maximum, square_minimum, square_diff, logits, training_op],
                        feed_dict={x: x_batch, y: y_batch})
                    epoch_mean = (epoch_mean + e_mean) / 2
                    epoch_maximum = max(epoch_maximum, e_max)
                    epoch_minimum = min(epoch_minimum, e_min)
                    epoch_diff.extend(e_diff)
                    epoch_out.extend(e_out)

                logger.log_scalar(tag='mean', value=epoch_mean, step=epoch)
                logger.log_scalar(tag='max', value=epoch_maximum, step=epoch)
                logger.log_scalar(tag='min', value=epoch_minimum, step=epoch)
                logger.log_histogram(tag='diff', values=epoch_diff, step=epoch, bins=int(np.amax(epoch_diff)))
                # logger.log_histogram(tag='out', values=epoch_out, step=epoch, bins=int(np.amax(epoch_out)))
                saver.save(session, outpath + '/train/model.ckpt', global_step=epoch)
            logger.log_scalar(tag='time', value=(time.time() - start_time), step=epoch)
            print("%s seconds" % (time.time() - start_time))

    elif mode == 'eval':
        with tf.Session() as session:
            logger = Logger(outpath + '/eval/', graph=session.graph)
            high_path = lowpath.replace('low', 'high')

            low_images = {}
            high_images = {}

            for filename in os.listdir(lowpath):
                low_images[filename] = misc.imread(lowpath + filename, mode='YCbCr')
                high_images[filename] = misc.imread(high_path + filename, mode='YCbCr')

            cubic_diff = []
            for filename, low_image in low_images.items():
                cubic_image = misc.imresize(low_image, 2.0, interp='bicubic')[:, :, 0]
                orig_image = high_images[filename][:, :, 0]
                cubic_diff.extend(utils.abs_diff(cubic_image, orig_image).reshape(-1))

                # logger.log_histogram(tag="%s/cubic" % filename, values=cubic_image[:, :, 0], step=0, bins=256)
                # logger.log_histogram(tag="%s/orig" % filename, values=orig_image[:, :, 0], step=0, bins=256)

            logger.log_scalar(tag='cubic_max', value=np.amax(cubic_diff), step=0)
            logger.log_scalar(tag='cubic_min', value=np.amin(cubic_diff), step=0)
            logger.log_scalar(tag='cubic_mean', value=np.mean(cubic_diff), step=0)
            logger.log_histogram(tag='cubic_diff', values=cubic_diff, step=0, bins=int(np.amax(cubic_diff)))
            #     logger.log_images(tag=filename, step=0, mode='YCbCr',
            #                       images=[cubic_image, orig_image],
            #                       tags=['cubic', 'orig'])
            #     logger.log_images(tag=filename, step=0, mode='L',
            #                       images=[cubic_image[:, :, 0], orig_image[:, :, 0]],
            #                       tags=['cubic_bw', 'orig_bw'])

            start_time = time.time()
            for epoch in range(n_epochs):
                saver.restore(session, "%s/train/model.ckpt-%s" % (outpath, epoch))
                epoch_maximum = 0.0
                epoch_minimum = 0.0
                epoch_mean = 0.0

                epoch_diff = []
                epoch_out = []

                epoch_diff_corr = []
                epoch_diff_hist = []
                epoch_diff_corr2 = []

                for filename, low_image in low_images.items():
                    low_patches = utils.subimages(low_image[:, :, 0])
                    orig_image = high_images[filename][:, :, 0]
                    high_patches = utils.subimages(orig_image, submat_shape=(20, 20))

                    low_patches = list(map(lambda x: x.reshape(-1), low_patches))
                    high_patches = list(map(lambda x: x.reshape(-1), high_patches))

                    e_mean, e_max, e_min, e_diff, Z = session.run(
                        [square_mean, square_maximum, square_minimum, square_diff, logits],
                        feed_dict={x: low_patches, y: high_patches})

                    epoch_mean = (epoch_mean + e_mean) / 2
                    epoch_maximum = max(epoch_maximum, e_max)
                    epoch_minimum = min(epoch_minimum, e_min)
                    epoch_diff.extend(e_diff)
                    epoch_out.extend(Z)

                    Z = np.around(list(map(lambda x: np.reshape(x, (-1, 20)), Z)))

                    shape_x, shape_y = low_image[:, :, 0].shape
                    super_img = utils.superimage(Z, (shape_x * 2, shape_y * 2))[..., None]

                    hist_img = utils.hist_match(super_img, low_image)
                    hist2_img = np.asarray(super_img)
                    img_minimum = np.amin(hist2_img)
                    if img_minimum < 0:
                        hist2_img = hist2_img + abs(img_minimum)
                    img_maximum = np.amax(hist2_img)
                    if img_maximum > 255:
                        hist2_img = hist2_img * (255 / img_maximum)
                    super_img[super_img < 0] = 0
                    super_img[super_img > 255] = 255

                    epoch_diff_corr.extend(utils.abs_diff(orig_image, super_img[:, :, 0]).reshape(-1))
                    epoch_diff_hist.extend(utils.abs_diff(orig_image, hist_img[:, :, 0]).reshape(-1))
                    epoch_diff_corr2.extend(utils.abs_diff(orig_image, hist2_img[:, :, 0]).reshape(-1))

                    # bi_Cb = misc.imresize(low_image[:, :, 1], 2.0, interp='bicubic')[..., None]
                    # bi_Cr = misc.imresize(low_image[:, :, 2], 2.0, interp='bicubic')[..., None]

                    # super_img = np.append(super_img, bi_Cb, axis=2)
                    # super_img = np.append(super_img, bi_Cr, axis=2)
                    # hist_img = np.append(hist_img, bi_Cb, axis=2)
                    # hist_img = np.append(hist_img, bi_Cr, axis=2)

                    # hist_diff = np.sqrt(np.square(hist_img[:, :, 0] - orig_image))
                    # super_diff = np.sqrt(np.square(super_img[:, :, 0] - orig_image))
                    # epoch_hist_maximum = max(np.amax(hist_diff), epoch_hist_maximum)
                    # epoch_hist_minimum = min(np.amin(hist_diff), epoch_hist_minimum)
                    # epoch_hist_mean = (np.mean(hist_diff) + epoch_hist_mean) / 2

                    # logger.log_histogram(tag="%s/super" % filename, values=super_img, step=epoch,
                    #                     bins=int(np.amax(super_img)))
                    # logger.log_histogram(tag="%s/hist" % filename, values=hist_img, step=epoch,
                    #                     bins=int(np.amax(hist_img)))
                    # logger.log_histogram(tag="%s/hist2" % filename, values=hist2_img, step=epoch,
                    #                     bins=int(np.amax(hist2_img)))
                    # logger.log_images(tag=filename, images=[super_img, hist_img], tags=['super', 'hist'], step=epoch,
                    #                   mode='YCbCr')
                    # logger.log_images(tag=filename, step=epoch, mode='L',
                    #                   images=[super_img[:, :, 0], super_diff, hist_img[:, :, 0], hist_diff],
                    #                   tags=['super_bw', 'super_orig_diff', 'hist_bw', 'hist_orig_diff'])

                logger.log_histogram(tag='diff_corr', values=epoch_diff_corr, step=epoch,
                                     bins=int(np.amax(epoch_diff_corr)))
                logger.log_histogram(tag='diff_hist', values=epoch_diff_hist, step=epoch,
                                     bins=int(np.amax(epoch_diff_hist)))
                logger.log_histogram(tag='diff_corr2', values=epoch_diff_corr2, step=epoch,
                                     bins=int(np.amax(epoch_diff_corr2)))
                logger.log_histogram(tag='diff', values=epoch_diff, step=epoch, bins=int(np.amax(epoch_diff)))
                # logger.log_histogram(tag='out', values=epoch_out, step=epoch, bins=int(np.amax(epoch_out)))
                logger.log_scalar(tag='min', value=epoch_minimum, step=epoch)
                logger.log_scalar(tag='max', value=epoch_maximum, step=epoch)
                logger.log_scalar(tag='mean', value=epoch_mean, step=epoch)
                logger.log_scalar(tag='hist_max', value=np.amax(epoch_diff_hist), step=epoch)
                logger.log_scalar(tag='hist_min', value=np.amin(epoch_diff_hist), step=epoch)
                logger.log_scalar(tag='hist_mean', value=np.mean(epoch_diff_hist), step=epoch)
                logger.log_scalar(tag='corr_max', value=np.amax(epoch_diff_corr), step=epoch)
                logger.log_scalar(tag='corr_min', value=np.amin(epoch_diff_corr), step=epoch)
                logger.log_scalar(tag='corr_mean', value=np.mean(epoch_diff_corr), step=epoch)
                logger.log_scalar(tag='corr2_max', value=np.amax(epoch_diff_corr2), step=epoch)
                logger.log_scalar(tag='corr2_min', value=np.amin(epoch_diff_corr2), step=epoch)
                logger.log_scalar(tag='corr2_mean', value=np.mean(epoch_diff_corr2), step=epoch)
            logger.log_scalar(tag='time', value=(time.time() - start_time), step=epoch)
            print("%s seconds" % (time.time() - start_time))

    elif mode == 'use':
        with tf.Session() as session:
            saver.restore(session, "%s/train/model.ckpt-%s" % (outpath, 999))
            for filename in os.listdir('images/test/low/'):
                save_path = "images/out/%s_%s.jpg"
                path = "images/test/low/%s" % filename
                high_path = path.replace('low', 'high')
                low_image = misc.imread(path, mode='YCbCr')
                orig_image = misc.imread(high_path, mode='YCbCr')
                bi_Cb = misc.imresize(low_image[:, :, 1], 2.0, interp='bicubic')[..., None]
                bi_Cr = misc.imresize(low_image[:, :, 2], 2.0, interp='bicubic')[..., None]

                cubic_image = misc.imresize(low_image, 2.0, interp='bicubic')
                cubic_diff = utils.abs_diff(cubic_image[:, :, 0], orig_image[:, :, 0])
                misc.toimage(orig_image[:, :, 0]).save(save_path % (filename, 'orig_bw'))
                misc.toimage(cubic_diff).save(save_path % (filename, 'cubic_diff'))
                misc.toimage(cubic_image, mode='YCbCr').save(save_path % (filename, 'cubic'))

                low_patches = utils.subimages(low_image[:, :, 0])
                high_patches = utils.subimages(orig_image[:, :, 0], submat_shape=(20, 20))

                low_patches = list(map(lambda x: x.reshape(-1), low_patches))
                high_patches = list(map(lambda x: x.reshape(-1), high_patches))

                Z = session.run(logits, feed_dict={x: low_patches, y: high_patches})

                Z = np.around(list(map(lambda x: np.reshape(x, (-1, 20)), Z)))
                shape_x, shape_y = low_image[:, :, 0].shape

                super_img = utils.superimage(Z, (shape_x * 2, shape_y * 2))[..., None]
                super_img1 = np.copy(super_img)

                super_img1 = np.append(super_img1, bi_Cb, axis=2)
                super_img1 = np.append(super_img1, bi_Cr, axis=2)
                misc.toimage(super_img1, mode='YCbCr').save(save_path % (filename, 'super'))
                misc.toimage(super_img1[:, :, 0]).save(save_path % (filename, 'super_bw'))

                super_diff = utils.abs_diff(super_img[:, :, 0], orig_image[:, :, 0])
                misc.toimage(super_diff).save(save_path % (filename, 'super_diff'))

                hist2_img = np.copy(super_img)
                img_minimum = np.amin(hist2_img)
                if img_minimum < 0:
                    hist2_img = hist2_img + abs(img_minimum)
                img_maximum = np.amax(hist2_img)
                if img_maximum > 255:
                    hist2_img = hist2_img * (255 / img_maximum)

                hist2_diff = utils.abs_diff(hist2_img[:, :, 0], orig_image[:, :, 0])
                misc.toimage(hist2_diff).save(save_path % (filename, 'hist_diff'))

                super_img[super_img < 0] = 0
                super_img[super_img > 255] = 255

                super_diff2 = utils.abs_diff(super_img[:, :, 0], orig_image[:, :, 0])
                misc.toimage(super_diff2).save(save_path % (filename, 'super2_diff'))
                misc.toimage(super_img[:, :, 0]).save(save_path % (filename, 'super2_bw'))
                super_img = np.append(super_img, bi_Cb, axis=2)
                super_img = np.append(super_img, bi_Cr, axis=2)
                misc.toimage(super_img, mode='YCbCr').save(save_path % (filename, 'super2'))
                misc.toimage(hist2_img[:, :, 0]).save(save_path % (filename, 'hist_bw'))
                hist2_img = np.append(hist2_img, bi_Cb, axis=2)
                hist2_img = np.append(hist2_img, bi_Cr, axis=2)
                misc.toimage(hist2_img, mode='YCbCr').save(save_path % (filename, 'hist'))


nn('use')
# nn('eval')
