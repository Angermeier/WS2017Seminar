import os

import numpy as np
import scipy.misc as misc


def hist_match(source, template):
    """
    Adjust the pixel values of a grayscale image such that its histogram
    matches that of a target image

    Arguments:
    -----------
        source: np.ndarray
            Image to transform; the histogram is computed over the flattened
            array
        template: np.ndarray
            Template image; can have different dimensions to source
    Returns:
    -----------
        matched: np.ndarray
            The transformed output image
    """

    oldshape = source.shape
    source = source.ravel()
    template = template.ravel()

    # get the set of unique pixel values and their corresponding indices and
    # counts
    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True,
                                            return_counts=True)
    t_values, t_counts = np.unique(template, return_counts=True)

    # take the cumsum of the counts and normalize by the number of pixels to
    # get the empirical cumulative distribution functions for the source and
    # template images (maps pixel value --> quantile)
    s_quantiles = np.cumsum(s_counts).astype(np.float64)
    s_quantiles /= s_quantiles[-1]
    t_quantiles = np.cumsum(t_counts).astype(np.float64)
    t_quantiles /= t_quantiles[-1]

    # interpolate linearly to find the pixel values in the template image
    # that correspond most closely to the quantiles in the source image
    interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

    return interp_t_values[bin_idx].reshape(oldshape)


def hist_match2(source, template, nbr_bins=255):
    if len(source.shape) < 3:
        source = source[:, :, np.newaxis]
        template = template[:, :, np.newaxis]

    imres = source.copy()
    for d in range(source.shape[2]):
        imhist, bins = np.histogram(source[:, :, d].flatten(), nbr_bins, normed=True)
        tinthist, bins = np.histogram(template[:, :, d].flatten(), nbr_bins, normed=True)

        cdfsrc = imhist.cumsum()  # cumulative distribution function
        cdfsrc = (255 * cdfsrc / cdfsrc[-1]).astype(np.uint8)  # normalize

        cdftint = tinthist.cumsum()  # cumulative distribution function
        cdftint = (255 * cdftint / cdftint[-1]).astype(np.uint8)  # normalize

        im2 = np.interp(source[:, :, d].flatten(), bins[:-1], cdfsrc)

        im3 = np.interp(im2, cdftint, bins[:-1])

        imres[:, :, d] = im3.reshape((source.shape[0], source.shape[1]))

    return imres


def subimages(image, submat_shape=(10, 10), stride=2):
    toreturn = list()

    x_test = image.shape[0] // stride % submat_shape[0] == 0
    y_test = image.shape[1] // stride % submat_shape[1] == 0

    for x in range((image.shape[0] // submat_shape[0]) * stride - (1 if x_test else 0)):
        for y in range((image.shape[1] // submat_shape[1]) * stride - (1 if y_test else 0)):
            x_st = x * (submat_shape[0] // 2)
            y_st = y * (submat_shape[1] // 2)
            toreturn.append(image[x_st:x_st + submat_shape[0], y_st:y_st + submat_shape[0]])
    return toreturn


def superimage(image_slices, image_size, stride=2):
    toreturn = np.empty(image_size)
    toreturn.fill(-1)

    x_test = (image_size[0] // stride) % image_slices[0].shape[0] == 0
    y_test = (image_size[1] // stride) % image_slices[0].shape[1] == 0

    x_patch_dim = image_slices[0].shape[0]
    y_patch_dim = image_slices[0].shape[1]
    x_dim = (image_size[0] // x_patch_dim) * stride - (1 if x_test else 0)
    y_dim = (image_size[1] // y_patch_dim) * stride - (1 if y_test else 0)

    for x in range(x_dim):
        for y in range(y_dim):
            x_st = x * (x_patch_dim // 2)
            y_st = y * (y_patch_dim // 2)
            slice = image_slices[x * y_dim + y]
            for i in range(x_patch_dim):
                for j in range(y_patch_dim):
                    if toreturn[x_st + i, y_st + j] == -1:
                        toreturn[x_st + i, y_st + j] = slice[i, j]
                    else:
                        toreturn[x_st + i, y_st + j] += slice[i, j]
                        toreturn[x_st + i, y_st + j] /= 2
    return toreturn


def get_data(path):
    low = list()
    high = list()
    for filename in os.listdir(path + 'low/'):
        low += subimages(misc.imread(path + 'low/' + filename, mode='YCbCr')[:, :, 0])
        high += subimages(misc.imread(path + 'high/' + filename, mode='YCbCr')[:, :, 0], submat_shape=(20, 20))
    return low, high


def abs_diff(one, two):
    return np.abs(np.asarray(one) - np.asarray(two))
